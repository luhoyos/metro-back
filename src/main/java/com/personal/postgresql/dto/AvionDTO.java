package com.personal.postgresql.dto;

import java.util.Date;

public class AvionDTO {
	
	
	private long id;
	private String nombre;
	private Date fechaDespegue;
	private Date fechaAterrizaje;
	
}
