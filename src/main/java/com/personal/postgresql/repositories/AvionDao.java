package com.personal.postgresql.repositories;

import org.springframework.data.repository.CrudRepository;

import com.personal.postgresql.entities.Avion;

public interface AvionDao extends CrudRepository<Avion, Long> {

}
